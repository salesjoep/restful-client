package fontys.sot.rest.client;

import fontys.sot.rest.client.school.model.Pants;
import fontys.sot.rest.client.school.model.Shop;
import fontys.sot.rest.client.school.model.Sweater;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Path("/pantstest")
public class SweaterClient {
    @Context
    private final WebTarget serviceTarget;
    private static final List<Shop> shopList = new ArrayList<>();

    public SweaterClient() {
        ClientConfig config = new ClientConfig();
        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        URI baseURI = UriBuilder.fromUri("http://localhost:9090/store/sweaters").build();
        serviceTarget = client.target(baseURI);

        Shop zalando = new Shop("ZA", "Zalando");
        Shop hema = new Shop("HE", "Hema");
        Shop jackandjones = new Shop("JJ", "Jack & Jones");
        shopList.add(zalando);
        shopList.add(hema);
        shopList.add(jackandjones);
    }

    public static void main(String[] args){
        SweaterClient client = new SweaterClient();
        client.getHello();
        client.getSweaterPath(2);
        client.getAllSweaters();
        client.getAllSweaters(shopList.get(1).getCode());
        client.deleteSweater(2);
        client.createSweater(5);
        client.updateSweaterJSON();
        client.updateSweaterForm(1);
    }
    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public void getHello(){

        Invocation.Builder requestBuilder = serviceTarget.path("hello").request().accept(MediaType.TEXT_PLAIN);
        Response response = requestBuilder.get();

        if (response.getStatus() == 200) {
            String entity = response.readEntity(String.class);
            System.out.println("The resources response is: " + entity);

        } else {
            System.out.println("ERROR: Cannot get hello! " + response);
            String entity = response.readEntity(String.class);
            System.out.println(entity);
        }

    }

    private void getAllSweaters(){

        Invocation.Builder requestBuilder = serviceTarget.request().accept(MediaType.APPLICATION_JSON);
        Response response = requestBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {

            GenericType<ArrayList<Sweater>> genericType = new GenericType<>() {};
            ArrayList<Sweater> entity = response.readEntity(genericType);
            for (Sweater sweater : entity) {
                System.out.println("\t"+sweater);
            }

        } else {
            printError(response);
        }

    }
    private void getAllSweaters(String shopCode){

        Invocation.Builder requestBuilder = serviceTarget.queryParam("shop", shopCode).request().accept(MediaType.APPLICATION_JSON);
        Response response = requestBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {

            GenericType<ArrayList<Sweater>> genericType = new GenericType<>() {};
            ArrayList<Sweater> entity = response.readEntity(genericType);
            for (Sweater sweater : entity) {
                System.out.println("\t"+ sweater);
            }

        } else {
            printError(response);
        }

    }
    private void getSweaterPath(int stNr){

        Invocation.Builder requestBuilder = serviceTarget.path(Integer.toString(stNr)).request()
                .accept(MediaType.APPLICATION_JSON);
        Response response = requestBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {

            Pants entity = response.readEntity(Pants.class);
            System.out.println("The resources response is: " + entity);

        } else {
            printError(response);
        }

    }

    private void printError(Response response){
        System.out.println("An ERROR occurred " + response);
        int statusCode = response.getStatus();
        if (statusCode>=400 && statusCode < 500){
            System.out.println("The client is to blame for the error.");
        }
        if (statusCode >= 500){
            System.out.println("The service is to blame for the error.");
        }
        String entity = response.readEntity(String.class);
        System.out.println("Error message is: "+entity);
    }


    private void deleteSweater(int stNr){

        WebTarget resourceTarget = serviceTarget.path(Integer.toString(stNr));
        Invocation.Builder requestBuilder = resourceTarget.request().accept(MediaType.TEXT_PLAIN);
        Response response = requestBuilder.delete();

        if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
            System.out.println("Deleted sweater with id 2 succesfully.");

        } else {
            printError(response);
        }
    }

    public void createSweater(int sweaterNumber) {

        Sweater sweater = new Sweater(sweaterNumber, "Black hoodie", shopList.get(2));
        Entity<Sweater> entity = Entity.entity(sweater, MediaType.APPLICATION_JSON);

        Response response = serviceTarget.request().accept(MediaType.TEXT_PLAIN).post(entity);

        if (response.getStatus() == Response.Status.CREATED.getStatusCode()) {
            String sweaterUrl = response.getHeaderString("Location");
            System.out.println("POST sweater is created and can be accessed at: " + sweaterUrl);
        } else {
            printError(response);
        }

    }

    public void updateSweaterJSON() {

        Sweater sweater = new Sweater(3, "Ripped sweater", shopList.get(1));
        Entity<Sweater> entity = Entity.entity(sweater, MediaType.APPLICATION_JSON);

        Response response = serviceTarget.path(Integer.toString(sweater.getSweaterNumber())).request().accept(MediaType.TEXT_PLAIN).put(entity);

        if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
            System.out.println("PUT sweater is updated");
        } else {
            printError(response);
        }
    }


    public void updateSweaterForm(int stNr) {
        Form form = new Form();
        form.param("name", "Jogger");
        form.param("shop", shopList.get(2).getCode());
        Entity<Form> entity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED);

        Response response = serviceTarget.path(Integer.toString(stNr)).request().accept(MediaType.TEXT_PLAIN).put(entity);

        if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
            System.out.println("PUT sweater is updated");
        } else {
            printError(response);
        }
    }
}
