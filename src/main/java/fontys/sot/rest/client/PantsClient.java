package fontys.sot.rest.client;

import fontys.sot.rest.client.school.model.Pants;
import fontys.sot.rest.client.school.model.Shop;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Path("/pantstest")
public class PantsClient {
    @Context
    private final WebTarget serviceTarget;
    private static final List<Shop> shopList = new ArrayList<>();

    public PantsClient() {
        ClientConfig config = new ClientConfig();
        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        URI baseURI = UriBuilder.fromUri("http://localhost:9090/store/pants").build();
        serviceTarget = client.target(baseURI);

        Shop zalando = new Shop("ZA", "Zalando");
        Shop hema = new Shop("HE", "Hema");
        Shop jackandjones = new Shop("JJ", "Jack & Jones");
        shopList.add(zalando);
        shopList.add(hema);
        shopList.add(jackandjones);
    }

    public static void main(String[] args){
        PantsClient client = new PantsClient();
        client.getHello();
        client.getPantsPath(2);
        client.getAllPants();
        client.getAllPants(shopList.get(1).getCode());
        client.deletePants(2);
        client.createPants(5);
        client.updatePantsJSON();
        client.updatePantsForm(1);
    }
    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public void getHello(){

        Invocation.Builder requestBuilder = serviceTarget.path("hello").request().accept(MediaType.TEXT_PLAIN);
        Response response = requestBuilder.get();

        if (response.getStatus() == 200) {
            String entity = response.readEntity(String.class);
            System.out.println("The resources response is: " + entity);

        } else {
            System.out.println("ERROR: Cannot get hello! " + response);
            String entity = response.readEntity(String.class);
            System.out.println(entity);
        }

    }

    private void getAllPants(){

        Invocation.Builder requestBuilder = serviceTarget.request().accept(MediaType.APPLICATION_JSON);
        Response response = requestBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {

            GenericType<ArrayList<Pants>> genericType = new GenericType<>() {};
            ArrayList<Pants> entity = response.readEntity(genericType);
            for (Pants pants : entity) {
                System.out.println("\t"+pants);
            }

        } else {
            printError(response);
        }

    }
    private void getAllPants(String shopCode){

        Invocation.Builder requestBuilder = serviceTarget.queryParam("shop", shopCode).request().accept(MediaType.APPLICATION_JSON);
        Response response = requestBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {

            GenericType<ArrayList<Pants>> genericType = new GenericType<>() {};
            ArrayList<Pants> entity = response.readEntity(genericType);
            for (Pants pants : entity) {
                System.out.println("\t"+ pants);
            }

        } else {
            printError(response);
        }

    }
    private void getPantsPath(int stNr){

        Invocation.Builder requestBuilder = serviceTarget.path(Integer.toString(stNr)).request()
                .accept(MediaType.APPLICATION_JSON);
        Response response = requestBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {

            Pants entity = response.readEntity(Pants.class);
            System.out.println("The resources response is: " + entity);

        } else {
            printError(response);
        }

    }

    private void printError(Response response){
        System.out.println("An ERROR occurred " + response);
        int statusCode = response.getStatus();
        if (statusCode>=400 && statusCode < 500){
            System.out.println("The client is to blame for the error.");
        }
        if (statusCode >= 500){
            System.out.println("The service is to blame for the error.");
        }
        String entity = response.readEntity(String.class);
        System.out.println("Error message is: "+entity);
    }


    private void deletePants(int stNr){

        WebTarget resourceTarget = serviceTarget.path(Integer.toString(stNr));
        Invocation.Builder requestBuilder = resourceTarget.request().accept(MediaType.TEXT_PLAIN);
        Response response = requestBuilder.delete();

        if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
            System.out.println("Deleted pants with id 2 succesfully.");

        } else {
            printError(response);
        }
    }

    public void createPants(int pantsNumber) {

        Pants pants = new Pants(pantsNumber, "Black Jeans", shopList.get(2));
        Entity<Pants> entity = Entity.entity(pants, MediaType.APPLICATION_JSON);

        Response response = serviceTarget.request().accept(MediaType.TEXT_PLAIN).post(entity);

        if (response.getStatus() == Response.Status.CREATED.getStatusCode()) {
            String pantsUrl = response.getHeaderString("Location");
            System.out.println("POST pants is created and can be accessed at: " + pantsUrl);
        } else {
            printError(response);
        }

    }

    public void updatePantsJSON() {

        Pants pants = new Pants(3, "Ripped Jeans", shopList.get(1));
        Entity<Pants> entity = Entity.entity(pants, MediaType.APPLICATION_JSON);

        Response response = serviceTarget.path(Integer.toString(pants.getPantsNumber())).request().accept(MediaType.TEXT_PLAIN).put(entity);

        if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
            System.out.println("PUT pants is updated");
        } else {
            printError(response);
        }
    }


    public void updatePantsForm(int stNr) {
        Form form = new Form();
        form.param("name", "Jogger");
        form.param("shop", shopList.get(2).getCode());
        Entity<Form> entity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED);

        Response response = serviceTarget.path(Integer.toString(stNr)).request().accept(MediaType.TEXT_PLAIN).put(entity);

        if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
            System.out.println("PUT pants is updated");
        } else {
            printError(response);
        }
    }
}
