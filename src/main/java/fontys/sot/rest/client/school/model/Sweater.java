package fontys.sot.rest.client.school.model;

import java.util.Objects;

public class Sweater {
    private int sweaterNumber;
    private String name;
    private Shop shop;

    public Sweater(int sweaterNumber, String name, Shop shop) {
        this.sweaterNumber = sweaterNumber;
        this.name = name;
        this.shop = shop;
    }

    public Sweater() {
    }



    public int getSweaterNumber() {
        return sweaterNumber;
    }

    public void setSweaterNumber(int sweaterNumber) {
        this.sweaterNumber = sweaterNumber;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sweater student = (Sweater) o;
        return sweaterNumber == student.sweaterNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sweaterNumber);
    }

    @Override
    public String toString() {
        return "Sweater{" +
                "sweaterNumber=" + sweaterNumber +
                ", name='" + name + '\'' +
                ", shop=" + shop +
                '}';
    }
}
