package fontys.sot.rest.client.school.model;

import java.util.Objects;

public class Shop {
    private String code;
    private String name;

    public Shop(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public Shop() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shop shop = (Shop) o;
        return Objects.equals(code, shop.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public String toString() {
        return "Shop{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
