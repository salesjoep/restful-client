package fontys.sot.rest.client.school.model;

import java.util.Objects;

@SuppressWarnings("WeakerAccess")
public class Pants {
    private int pantsNumber;
    private String name; // full name, e.g., "Joe Smith"
    private Shop shop; // country where the students comes from

    public Pants(int pantsNumber, String name, Shop shop) {
        this.pantsNumber = pantsNumber;
        this.name = name;
        this.shop = shop;
    }

    public Pants() {
    }



    public int getPantsNumber() {
        return pantsNumber;
    }

    public void seyPantsNumber(int pantsNumber) {
        this.pantsNumber = pantsNumber;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pants pants = (Pants) o;
        return pantsNumber == pants.pantsNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pantsNumber);
    }

    @Override
    public String toString() {
        return "Pants{" +
                "pantsNumber=" + pantsNumber +
                ", name='" + name + '\'' +
                ", shop=" + shop +
                '}';
    }
}
